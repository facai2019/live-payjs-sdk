package payjs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain=true)
public abstract class PayInfo {
	/** 金额。单位：分 */
	private long total_fee;
	/** 用户端自主生成的订单号 */
	private String out_trade_no;
	/** 订单标题 */
	private String body;
	/**用户自定义数据，在notify的时候会原样返回*/
	private String attach;
	/**接收微信支付异步通知的回调地址。必须为可直接访问的URL，不能带参数、session验证、csrf验证。留空则不通知*/
	private String notify_url;
}
